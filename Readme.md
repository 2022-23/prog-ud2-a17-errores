# PROG-UD2-A16 Interpretació d'errors

En aquest exercici, hauràs d'anar arreglant els diferents errors que apareixen en cada un dels arxius.
Per fer-ho, A messura que avancis, hauràs d'anar descomentant aquell codi que et trobes comentat.

Per finalitzar, escriu amb un comentari dalt de la definició de cada clase el problema que has trobat
i si aquest és un problema que es dona en **temps d'execució** o en **temps de compilació**.

- **Error en temps de compilació:** errors de programació que impedeixen la compilació i execució del programa (síntaxis, sentències incorrectes,...)
- **Error en temps d'execució:** error de programació que ens donen un resultat erròni (operacions incorrectes, tipus no adients al seu contingut,...)


A continuació tens un exemple:

```java

/**
 * @Error: No podem Asignar un enter a una variable de tipus String
 * @Tipus: Temps de compilació.
 **/
public class Actividad16_XX {

    public static void main(String[] args) {
         int numero = "2023";
    }

}
'''